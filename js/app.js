// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.shares', {
    url: '/shares',
    views: {
      'menuContent': {
        templateUrl: 'templates/shares.html'
      }
    }
  })
    .state('app.shares-add', {
    url: '/shares-add',
    views: {
      'menuContent': {
        templateUrl: 'templates/shares-add.html'
      }

    }
  })
    .state('app.shares-show', {
    url: '/shares-show',
    views: {
      'menuContent': {
        templateUrl: 'templates/shares-show.html'
      }

    }
  })

  .state('app.options', {
      url: '/options',
      views: {
        'menuContent': {
          templateUrl: 'templates/options.html'
        }
      }
    })
      .state('app.options-add', {
    url: '/options-add',
    views: {
      'menuContent': {
        templateUrl: 'templates/options-add.html'
      }

    }
  })
    .state('app.options-show', {
    url: '/options-show',
    views: {
      'menuContent': {
        templateUrl: 'templates/options-show.html'
      }

    }
  })

  .state('app.warrants', {
      url: '/warrants',
      views: {
        'menuContent': {
          templateUrl: 'templates/warrants.html'
        }
      }
    })
        .state('app.warrants-add', {
    url: '/warrants-add',
    views: {
      'menuContent': {
        templateUrl: 'templates/warrants-add.html'
      }

    }
  })
    .state('app.warrants-show', {
    url: '/warrants-show',
    views: {
      'menuContent': {
        templateUrl: 'templates/warrants-show.html'
      }

    }
  })

    .state('app.convertible', {
      url: '/convertible',
      views: {
        'menuContent': {
          templateUrl: 'templates/convertible.html'
        }
      }
    })
      .state('app.convertible-add', {
    url: '/convertible-add',
    views: {
      'menuContent': {
        templateUrl: 'templates/convertible-add.html'
      }

    }
  })
    .state('app.convertible-show', {
    url: '/convertible-show',
    views: {
      'menuContent': {
        templateUrl: 'templates/convertible-show.html'
      }

    }
  })
    .state('app.home', {
      url: '/home',
      views: {
        'menuContent': {
          templateUrl: 'templates/home.html',
          controller: 'AppCtrl'
        }
      }
    })
  .state('app.single-view', {
      url: '/single-view',
      views: {
        'menuContent': {
          templateUrl: 'templates/single-view.html'
        }
      }
    })

    .state('app.summary-details', {
      url: '/summary-details',
      views: {
        'menuContent': {
          templateUrl: 'templates/summary-details.html'
        }
      }
    })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});
